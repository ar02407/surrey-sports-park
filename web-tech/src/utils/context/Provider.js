import React, { useState } from "react";

export const context = React.createContext(null);

export default function Provider({ children }) {
  //context states
  const [selectedSport, setSelectedSport] = useState(null);
  const [refreshSport, setRefreshSport] = useState(false);
  const [admin, setAdmin] = useState(null);
  //store
  const store = {
    data: { selectedSport, refreshSport, admin },
    action: { setSelectedSport, setRefreshSport, setAdmin },
  };
  //provider component
  return <context.Provider value={store}>{children}</context.Provider>;
}
