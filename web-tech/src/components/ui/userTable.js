import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

export default function UserTable({ rows }) {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell
              style={{ fontSize: "1.15rem", fontWeight: "bold" }}
              align="left"
            >
              StudentId
            </TableCell>
            <TableCell
              style={{ fontSize: "1.15rem", fontWeight: "bold" }}
              align="left"
            >
              Name
            </TableCell>
            <TableCell
              style={{ fontSize: "1.15rem", fontWeight: "bold" }}
              align="left"
            >
              Email
            </TableCell>
            <TableCell
              style={{ fontSize: "1.15rem", fontWeight: "bold" }}
              align="left"
            >
              Gender
            </TableCell>
            <TableCell
              style={{ fontSize: "1.15rem", fontWeight: "bold" }}
              align="left"
            >
              Age
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              align="left"
              key={row.name}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" className="table_cell">
                {row.studentId}
              </TableCell>
              <TableCell align="left" className="table_cell">
                {row.name}
              </TableCell>
              <TableCell align="left" className="table_cell">
                {row.email}
              </TableCell>
              <TableCell align="left" className="table_cell">
                {row.gender}
              </TableCell>
              <TableCell align="left" className="table_cell">
                {row.age}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
