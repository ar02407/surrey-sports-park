"use client";
import Navbar from "../navbar";
import "../../../App.css";
import { useContext, useEffect } from "react";
import { context } from "../../../utils/context/Provider";
import { useNavigate } from "react-router-dom";

export default function Layout({ children }) {
  const store = useContext(context);
  const router = useNavigate();

  useEffect(() => {
    if (!store.data.admin) {
      router("/login");
    }
  }, [store.data.admin]);
  return (
    <div className="layout_container">
      <Navbar />
      {children}
    </div>
  );
}
