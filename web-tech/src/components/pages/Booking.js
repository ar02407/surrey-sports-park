import { useEffect, useState } from "react";
import server from "../../utils/server";
import BookTable from "../ui/bookTable";
import Layout from "../ui/Layout";

export default function Booking() {
  const [bookedData, setBookedData] = useState([]);

  const getBookedList = async () => {
    try {
      const res = await server.get("api/admin/booking");
      console.log("res data of booked list", res.data.data);
      setBookedData(res.data.data);
    } catch (err) {
      console.log("error", err);
    }
  };

  useEffect(() => {
    getBookedList();
  }, []);
  return (
    <Layout>
      <div
        style={{ padding: "2rem", maxWidth: "1200px", marginInline: "auto" }}
      >
        <h2 className="booking_title">
          {bookedData.length ? "Booking" : "No Booking found"}
        </h2>
        <div className="booking_list">
          {bookedData.length ? <BookTable rows={bookedData} /> : null}
        </div>
      </div>
    </Layout>
  );
}
