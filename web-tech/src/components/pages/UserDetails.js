"use client";
import Layout from "../ui/Layout";
import { useState, useEffect } from "react";
import server from "../../utils/server";
import UserTable from "../ui/userTable";

export default function UserDetails() {
  //user data
  const [users, setUsers] = useState([]);

  const getUsers = async () => {
    try {
      const res = await server.get("/api/user/all");
      setUsers(res.data.data);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getUsers();
  }, []);
  return (
    <Layout>
      <div
        style={{ padding: "2rem", maxWidth: "1200px", marginInline: "auto" }}
      >
        <div className="userdetails_title">User Details</div>
        <div className="userdetails_container">
          {/* <div className="userdetails_list"> */}
          {/*   {users.map((user) => ( */}
          {/*     <div className="userdetails_listitem"> */}
          {/*       <div>{user.studentId}</div> */}
          {/*       <div>{user.name}</div> */}
          {/*       <div>{user.email}</div> */}
          {/*       <div>{user.gender}</div> */}
          {/*       <div>{user.age}</div> */}
          {/*     </div> */}
          {/*   ))} */}
          {/* </div> */}
          <UserTable rows={users} />
        </div>
      </div>
    </Layout>
  );
}
