import express from "express";
import { PrismaClient } from "@prisma/client";
import router from "./routes/index.js";
import bodyParser from "body-parser";
import cors from "cors";
import { execSync } from 'child_process'

const prisma = new PrismaClient();

async function main() {
  //migrate the schema to the db
  try {
    execSync('npx prisma migrate deploy', { stdio: 'inherit' })
  } catch (err) {
    console.log(err)
  }
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });

const app = new express();
//request check
app.use(cors());

//accept the application/json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//router for the api
app.use("/api", router);

app.listen(4321, () => {
  console.log("app is listening to 4321");
});
