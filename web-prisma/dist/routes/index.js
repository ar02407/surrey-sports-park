"use strict";

function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _express = _interopRequireDefault(require("express"));
var controller = _interopRequireWildcard(require("../controllers/index.js"));
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != _typeof(e) && "function" != typeof e) return { "default": e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n["default"] = e, t && t.set(e, n), n; }
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
var router = _express["default"].Router();

/** sport api */
router.route("/admin/sport").post(controller.admin.create);
router.route("/admin/sport").get(controller.admin.find);
router.route("/admin/sport/:id").get(controller.admin.findOne);
router.route("/admin/sport").put(controller.admin.update);
router.route("/admin/sport/:id")["delete"](controller.admin.remove);
router.route("/admin/booking").post(controller.booking.create);
router.route("/admin/booking/:id").get(controller.booking.get);
router.route("/admin/booking").get(controller.booking.getAllBooking);
//user
router.route("/user/signup").post(controller.user.signup);
router.route("/user/login").post(controller.user.login);
router.route("/user/get/:id").get(controller.user.getUser);
router.route("/user/all").get(controller.user.getAllUser);
var _default = exports["default"] = router;