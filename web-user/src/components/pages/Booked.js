import Layout from "../ui/Layout";
import * as React from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import { useEffect, useState } from "react";
import server from "../../utils/server";
import { context } from "../../utils/context/Provider";
import BasicTable from "../ui/BasicTable";
import useSessionData from "../../utils/hooks/useSessionData";

export default function Booked() {
  //state
  const [bookedData, setBookedData] = useState([]);
  const store = React.useContext(context);
  const getuser = useSessionData('user');
  //get all the booked list data
  const getBookedList = async () => {
    const id = getuser.data.id;
    try {
      const res = await server.get
        (`/api/admin/booking/${id}`);
      console.log("res", res.data.data);
      setBookedData(res.data.data);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    //get the all ooked data
    if (getuser.data?.id) {
      getBookedList();
    }
  }, [getuser.data?.id]);
  return (
    <Layout>
      <div style={{ padding: "2rem", maxWidth: "1200px", marginInline: "auto" }}>
        <h2 className="booked_title">{bookedData.length ? 'Booked List' : 'No Booked Details'}</h2>
        <div className="booked_list_flex">
          {bookedData.length ?
            <BasicTable rows={bookedData} />
            : null
          }
        </div>
      </div>
    </Layout>
  );
}
