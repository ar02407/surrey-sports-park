import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Avatar from "@mui/material/Avatar";

export default function BasicTable({ rows }) {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell
              style={{ fontSize: "1.15rem", fontWeight: "bold" }}
              align="left"
            >
              Image
            </TableCell>
            <TableCell
              style={{ fontSize: "1.15rem", fontWeight: "bold" }}
              align="left"
            >
              Name
            </TableCell>
            <TableCell
              style={{ fontSize: "1.15rem", fontWeight: "bold" }}
              align="left"
            >
              Sport
            </TableCell>
            <TableCell
              style={{ fontSize: "1.15rem", fontWeight: "bold" }}
              align="left"
            >
              Date
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              align="left"
              key={row.name}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" className="table_cell">
                <Avatar src={row.sport.image} />
              </TableCell>
              <TableCell align="left" className="table_cell">
                {row.user.name}
              </TableCell>
              <TableCell align="left" className="table_cell">
                {row.sport.name}
              </TableCell>
              <TableCell align="left" className="table_cell">
                {new Date(row.datetime).toLocaleString('uk')}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
