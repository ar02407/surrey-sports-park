import Navbar from "../navbar";
import "../../../App.css";
import "../../../components/pages/index.css";
import useSessionData from "../../../utils/hooks/useSessionData";
import { useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { context } from "../../../utils/context/Provider";

export default function Layout({ children }) {
  //get use session
  const user = useSessionData('user');
  const store = useContext(context);
  //get navigation
  const router = useNavigate();

  useEffect(() => {
    //check if the user exist in the session storage 
    //else logout the user
    console.log(store.data.userDetails, user.data)
    if (!(store.data.userDetails || user.data)) {
      router('/login')
    }
  }, [user.data, store.data.userDetails])

  return (
    <div className="layout_container">
      <Navbar />
      {children}
    </div>
  );
}
