import { useState, useEffect } from "react"

export default function useSessionData(key) {
  const [data, setData] = useState(null);
  useEffect(() => {
    const user = JSON.parse(sessionStorage.getItem(key));
    setData(user)
  }, [key])

  return { data }
}
