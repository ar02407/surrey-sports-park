import React, { useState } from "react";

export const context = React.createContext(null);

export default function Provider({ children }) {
  //context states
  const [selectedSport, setSelectedSport] = useState();
  const [refreshSport, setRefreshSport] = useState(false);
  const [userDetails, setUserDetails] = useState(null);
  //store
  const store = {
    data: { selectedSport, refreshSport, userDetails },
    action: { setSelectedSport, setRefreshSport, setUserDetails },
  };
  //provider component
  return <context.Provider value={store}>{children}</context.Provider>;
}
