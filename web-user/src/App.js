import Home from "./components/pages/Home";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Login from "./components/pages/Login";
import SportEdit from "./components/pages/SportEdit";
import SportCreate from "./components/pages/SportsCreate";
import { Toaster } from "react-hot-toast";
import Provider from "./utils/context/Provider";
import Booking from "./components/pages/Booking";
import Booked from "./components/pages/Booked";
import SignUp from "./components/pages/Signup";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/signup",
    element: <SignUp />,
  },
  {
    path: "/sportedit",
    element: <SportEdit />,
  },
  {
    path: "/createsport",
    element: <SportCreate />,
  },
  {
    path: "/booking",
    element: <Booking />,
  },
  {
    path: "/booked",
    element: <Booked />,
  },
]);

function App() {
  return (
    <div>
      <Provider>
        <RouterProvider router={router} />
        <Toaster />
      </Provider>
    </div>
  );
}

export default App;
